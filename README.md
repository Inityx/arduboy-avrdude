# arduboy-avrdude

This is an Arduboy sketch, compiled outside the Arduino IDE.

This project expects two symlinks in the repo root:

* /arduboy -> Arduboy2/src
* /arduino -> ~/.ArduinoXX/packages/arduino/hardware/avr/X.X.XX
