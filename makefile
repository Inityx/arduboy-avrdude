# Platform
PORT:=/dev/ttyACM1
BAUD:=57600
CPUFREQ:=16000000
MCU:=atmega32u4
USBVID:=2341
USBPID:=8036

# Source
MAINSRC:=src/sketch_main.cpp
MAINHEX:=build/main.hex
MAINELF:=$(MAINHEX:hex=elf)

# Arduboy
ARDUBOY_CPP:=$(wildcard arduboy2/*.cpp)
ARDUBOY_O:=$(addprefix build/, $(ARDUBOY_CPP:cpp=o))

# Arduino Core
ARDUINOCORE_CPP:=$(notdir $(wildcard arduino/cores/arduino/*.cpp))
ARDUINOCORE_CPP_O:=$(addprefix build/arduino_cpp/, $(notdir $(ARDUINOCORE_CPP:cpp=o)))

ARDUINOCORE_C:=$(notdir $(wildcard arduino/cores/arduino/*.c))
ARDUINOCORE_C_O:=$(addprefix build/arduino_c/, $(ARDUINOCORE_C:c=o))

ARDUINOCORE_ASM:=$(notdir $(wildcard arduino/cores/arduino/*.S))
ARDUINOCORE_ASM_O:=$(addprefix build/arduino_asm/, $(ARDUINOCORE_ASM:S=o))

ARDUINOCORE_OBJECTS:=$(ARDUINOCORE_CPP_O) $(ARDUINOCORE_C_O) $(ARDUINOCORE_ASM_O)

# Compilers
INCLUDEPATH=arduino/variants/leonardo arduino/cores/arduino arduino/libraries/EEPROM/src ./arduboy2
CFLAGS:=-DF_CPU=$(CPUFREQ)UL -DUSB_VID=$(USBVID) -DUSB_PID=$(USBPID) -mmcu=$(MCU) \
  -flto -fuse-linker-plugin -Wl,--gc-sections \
  $(addprefix -I, $(INCLUDEPATH))

CXX:=avr-g++ --std=c++14 -Os -fno-threadsafe-statics -fno-exceptions $(CFLAGS)
CC:=avr-gcc $(CFLAGS)

AVRDUDE:=avrdude -p$(MCU) -cavr109 -P$(PORT) -b$(BAUD) -v
SIZE:=avr-size
OBJ2HEX:=avr-objcopy -R .eeprom -O ihex

# Targets
all: $(MAINHEX)

upload: $(MAINHEX)
	stty -F $(PORT) 1200
	sleep 0.5
	$(AVRDUDE) -Uflash:w:$<:i

# Compile
$(MAINHEX): $(MAINELF)
	$(OBJ2HEX) $< $@

$(MAINELF): $(MAINSRC) $(ARDUINOCORE_OBJECTS) $(ARDUBOY_O) | build
	$(CXX) $< $(ARDUINOCORE_OBJECTS) $(ARDUBOY_O) -o $@
	$(SIZE) -A $@

build/arduino_cpp/%.o: arduino/cores/arduino/%.cpp | build/arduino_cpp
	$(CXX) -c $< -o $@
	
build/arduino_c/%.o: arduino/cores/arduino/%.c | build/arduino_c
	$(CC) -c $< -o $@

build/arduino_asm/%.o: arduino/cores/arduino/%.S | build/arduino_asm
	$(CC) -c $< -o $@

build/arduboy2/%.o: arduboy2/%.cpp | build/arduboy2
	$(CXX) -c $< -o $@

# Dirs
build build/arduboy2 build/arduino_cpp build/arduino_c build/arduino_asm:
	mkdir -p $@

# Util
clean:
	rm -rf ./build
