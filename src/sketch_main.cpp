#include <Arduboy2.h>

Arduboy2 arduboy;

void hello_world() {
    const int16_t width  = arduboy.frameCount / 5 % WIDTH;
    const int16_t height = arduboy.frameCount / 3 % HEIGHT;

    arduboy.setCursor(width, height);
    arduboy.print(F("Hello, world!"));
}

void setup() {
    arduboy.begin();
}

void loop() {
    if (!(arduboy.nextFrame())) return;

    hello_world();
    arduboy.display(true);
}
